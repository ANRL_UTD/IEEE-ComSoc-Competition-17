import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.*;
import java.util.Date;
import java.text.SimpleDateFormat;

// 127.0.17.1
public class Server {

    // for now we are not calling the 
    private static String DestinationPhone = "+19723022013"; // this is the phone number that is called
    private static String AuthorizedCallerPhone = "+14693221354"; // this is our registered phone number

    private static final int PORT = 43005;
    private static final int WEBSERVER_PORT = 43006; // port number of NodeJS server

    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/traffic";

    //  Database credentials
    static final String USER = "anrl";
    static final String PASS = "";

    private static Connection conn = null;
    private static Statement stmt = null;

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // to change the date format to MySQL format

    public static void main(String[] args) throws Exception {
        System.out.println("Retrieving state...");
        System.out.println("Running server...");

        try {
            //STEP 2: Register JDBC driver
            Class.forName("com.mysql.jdbc.Driver");

            //STEP 3: Open a connection
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Connected to the specified database successfully...");
            stmt = conn.createStatement();
        } catch (SQLException se) {
            se.printStackTrace();
        }
        ServerSocket ServerSocketlistener = new ServerSocket(PORT);
        ServerSocket NodeJSSocketListener = new ServerSocket(WEBSERVER_PORT);

        try {

            NodeJSHandler NJSH = new NodeJSHandler(NodeJSSocketListener.accept());
            NJSH.start();
            new Handler(ServerSocketlistener, NJSH.getNodeJSSocket()).start();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
//            ServerSocketlistener.close();
        }
    }

    private static class NodeJSHandler extends Thread {

        private Socket socket;
        private PrintWriter out;

        public NodeJSHandler(Socket socket) {
            this.socket = socket;
        }

        public Socket getNodeJSSocket() {
            return socket;
        }

        public void run() {

            try {
                out = new PrintWriter(socket.getOutputStream(), true);
            } catch (IOException ex) {
                ex.printStackTrace();
            }

        }
    }

    private static class Handler extends Thread {

        private ServerSocket Serversocket;
        private Socket currentSocket;
        private Socket nodeJSSocket;
        private BufferedReader in;

        public Handler(ServerSocket Serversocket, Socket nodeJSSocket) {
            this.nodeJSSocket = nodeJSSocket;
            this.Serversocket = Serversocket;
        }

        public void run() {

            while (true) {

                try {
                    currentSocket = Serversocket.accept();
                    // Create character streams for socket
                    in = new BufferedReader(new InputStreamReader(currentSocket.getInputStream()));

                    System.out.println("Client connected!");
                    // Accept messages from this client and broadcast them
                    // Ignore other clients that cannot be broadcast to
                    String input;
                    while ((input = in.readLine()) != null) {
                        // [latitude0][space][longitude1][space][speed2][space][shock3][space][gforce4][space][axis5][space][type6][space][driverId7]
                        input = input.replaceAll("\\P{Print}", "");
                        if (input.length() > 10) {
                            System.out.println("Read: '" + input + "'");
                            String[] parts = input.split(" ");
                            String message;
                            if (parts[6].equalsIgnoreCase("1")) { // if type is 1 (Accident
                                message = generateMessage(Integer.parseInt(parts[4]), parts[5]);
                            } else {
                                message = "Pothole";
                            }
                            if (parts[0].startsWith("0.0") && parts[1].startsWith("0.0")) {
                                continue;
                            }
                            String humanReadableAddress = ReverseGeocode.getStreetAddress(parts[0], parts[1]);
                            int eventID = writeEventToDB(parts[0], parts[1], parts[2], parts[3], parts[6], parts[7], message, humanReadableAddress);

                            try {
                                sendEventIDtoNodeJSserver(nodeJSSocket, eventID, parts[0], parts[1], parts[2], parts[3], message, humanReadableAddress, parts[7]);
                            } catch (UnknownHostException e) {
                                System.out.println("Node server is down!");
                            }
                            if (parts[6].equalsIgnoreCase("1")) { // if type is 1 (Accident
                                callnTextEmergencyContacts(parts[7], parts[0], parts[1], humanReadableAddress);
                            }

                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                } finally {

                    try {
                        currentSocket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        /**
         * Sends the inserted event ID to NodeJS server
         * [id][space][latitude][space][longitude][space][speed][space][shock%][space][message][space]
         * [human readable address][space][driverID]
         *
         * @param eventID
         * @throws UnknownHostException
         */
        private static void sendEventIDtoNodeJSserver(Socket socket, int eventID, String latitude, String longitude, String speed, String shock, String message, String GeoReverseAddress, String driverId) throws UnknownHostException {
            try {
                // out & in
                PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                // writes str in the socket and read
                out.println(eventID + " " + latitude + " " + longitude + " " + speed + " " + shock + " " + message + " " + GeoReverseAddress + " " + driverId);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        /**
         * Writes the event to the DB, and returns the ID of the last inserted
         * event
         *
         * @param latitude
         * @param longitude
         * @param speed
         * @param shock
         * @param type
         * @param driverId
         * @return
         * @throws SQLException
         */
        private int writeEventToDB(String latitude, String longitude, String speed, String shock, String type, String driverId, String message, String address) throws SQLException {
//            int day = (int) genUniformRandomBetween(3, 15);
//            int hour = (int) genUniformRandomBetween(0, 24);
//            int min = (int) genUniformRandomBetween(0, 60);
//            Date date = new Date(117, 8, day, hour, min);
            Date date = new Date();
            String currentTime = sdf.format(date);
            String query = "INSERT INTO events (date, latitude, longitude, speed, shock, type, driverId, message, address)"
                    + " VALUES ('" + currentTime + "', '" + latitude + "', '" + longitude + "', '" + speed + "', '" + shock + "', '" + type + "', '" + driverId + "', '" + message + "', '" + address + "');";
            stmt.executeUpdate(query);
            // get the last inserted ID
            query = "SELECT LAST_INSERT_ID() as id;";
            ResultSet rs = stmt.executeQuery(query);
            rs.next();
            return rs.getInt("id");
        }

        private static void callnTextEmergencyContacts(String DriverID, String latitude, String longitude, String humanReadableAddress) throws SQLException, IOException {
            String sql = "SELECT * from drivers where id='" + DriverID + "'";
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                //Retrieve by column name
                String name = rs.getString("name");
                String EmergencyName = rs.getString("emergencyName");
                String EmergencyContact = rs.getString("emergencyContact");
                String plate = rs.getString("licensePlate");

                System.out.println("****ACCIDENT****");
                String emergencyMessage = "Hello, We are sorry to deliver this urgent message. " + name + ", driver of the car with licence plate number " + plate + " was involved in an accident now, at" + humanReadableAddress;
                System.out.println(emergencyMessage);
                System.out.println("Contacting 911 and emergency contact " + EmergencyName + " at " + EmergencyContact + "...");

                Calling.textPhone(DestinationPhone, emergencyMessage, AuthorizedCallerPhone); // text the emergency contact

                Calling.createXML(emergencyMessage, "/var/www/html/call/v1.xml");
                Calling.callPhone(DestinationPhone, "http://ec2-54-202-247-75.us-west-2.compute.amazonaws.com/call/v1.xml", AuthorizedCallerPhone);

            }
            rs.close();
        }

        /**
         * This function is not completely written yet. It's just a skeleton.
         *
         * @param shock
         * @param biggestShockAxis
         * @return
         */
        private static String generateMessage(int gforce, String biggestShockAxis) {
            String message = "Accident from the ";
            if (gforce < 0) {
                if (biggestShockAxis.equalsIgnoreCase("x")) {
                    message += "front";
                } else if (biggestShockAxis.equalsIgnoreCase("y")) {
                    message += "driver side";
                } else {
                    message += "top";
                }
            } else {
                if (biggestShockAxis.equalsIgnoreCase("x")) {
                    message += "back";
                } else if (biggestShockAxis.equalsIgnoreCase("y")) {
                    message += "passenger side";
                } else {
                    message += "bottom";
                }
            }
            return message;
        }

        /**
         * Generate a uniformly distributed random number between min and max
         *
         * @param min
         * @param max
         */
        public static double genUniformRandomBetween(double min, double max) {
            return min + (max - min) * Math.random();
        }

    }
}

