import java.io.*;
import java.net.*;

class TCPServer {
    public static void main(String argv[]) throws Exception {
        System.out.println("Running server");
	ServerSocket welcomeSocket = new ServerSocket(43005);
        Socket connectionSocket = welcomeSocket.accept();
        BufferedReader in = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
        DataOutputStream out = new DataOutputStream(connectionSocket.getOutputStream());
        String input;
        while (true) {
	    input = in.readLine();
            if (input != null) { 
                System.out.println(input);
	    }
        }
    }
}
