import java.io.*;
import java.net.*;
import java.util.*;

public class Server {

    public static void main(String[] args) throws IOException {
        System.out.println("Running server...\nPress [Enter] to fire a message to the client.\n(Note: if there is no response after pressing the [Enter] button, make sure a client is connected)");
	ServerSocket listener = new ServerSocket(43006);
        Socket socket = listener.accept(); 
        PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
        Scanner scanner = new Scanner(System.in);
        String input = "";
        try {
            while (true) {
                input = scanner.nextLine();
                out.println("1,32.988395,-96.752076,38.212,70%,Accident from the passenger side,1234 Main St.");
		out.println("3,32.988395,-96.752076,38.212,70%,Pothole,1234 Main St.");
                System.out.print("Send message to client");
                if (input.equalsIgnoreCase("Q")) {
                    socket.close();
                    break;
                }
            }
        }
        finally {
            listener.close();
        }
    }
}
