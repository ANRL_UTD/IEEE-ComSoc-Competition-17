const path = require('path')
const express = require('express')
const app = express()
const http = require('http')
const http_ = http.Server(app)
const io = require('socket.io')(http_)
const mysql = require('mysql')
const PORT = 80

let currentSQLEvents = [];


/**
 * Express framework
 */

app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8888');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

// Main page
app.get('/', function(req, res){
  res.sendFile(path.resolve('/home/ubuntu/website/index.html'))
})

// Console
app.get('/console', function(req, res, next){
  res.sendFile(path.resolve('/home/ubuntu/website/console.html'))
})

// Listen to port
http_.listen(PORT, function(){
  console.log(`NodeJS server listening on port ${PORT}!`)
})


/**
 * MySQL connection
 */

const sqlCon = mysql.createConnection({
  host: 'localhost',
  user: 'anrl',
  password: '',
  database: 'traffic'
})

// Establish connection to MySQL db 
sqlCon.connect(function(err){
  if(err) throw err
  console.log(`Connected to MySQL db!`)
})

// Query MySQL db
var sqlStmt = 'SELECT * from events WHERE date >= NOW() - INTERVAL 15 MINUTE;'
sqlCon.query(sqlStmt, function(err, result){
  if(err) throw err
  currentSQLRecords = result
})


/**
 * Socket.io
 */

// Client connection
io.on('connection', function(socket){
  console.log(`A client connected`)
 
  // Send current SQL event records to a new client that connects
  socket.emit('client-connection', currentSQLEvents)

  // Client disconnection
  socket.on('disconnect', function(){
    // Don't have to do anything right now
  })
})
