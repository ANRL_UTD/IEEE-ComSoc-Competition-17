#!/bin/bash
date=$(echo | sudo openssl x509 -noout -dates -in /etc/letsencrypt/live/project-ariadne.protobyte.info/cert.pem | sed -n -e '2{p;q}')
echo $date
now=$(TZ=GMT date)
echo $now
